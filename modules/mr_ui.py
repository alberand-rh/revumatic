import tuimatic

from modules.formatters import CommitFormatter

class DeletableEditor(tuimatic.Editor):
    signals = ['remove']

    def keypress(self, size, key):
        key = super().keypress(size, key)
        if not key:
            return
        if key in ('delete', 'backspace'):
            # backspace at the beginning of the text or delete at the end
            if len(self._text) == 1 and not self._text[0]:
                # empty text => request widget removal
                self._send_signal('remove')
                return
        return key


class HighlightedTextPile(tuimatic.TextPile):
    def __init__(self, widget_list, inactive_map=None):
        self.inactive_map = inactive_map
        super().__init__(widget_list)

    def subrender(self, size, origin_y, focus=False):
        canv = super().subrender(size, origin_y, focus)
        if not focus and self.inactive_map:
            canv.fill_attr_apply(self.inactive_map)
        return canv


class DiffstatViewer(tuimatic.Viewer):
    def __init__(self, stat):
        self._stat = stat
        super().__init__('', wrap='clip')

    def build_line_cache(self, maxcol):
        if self.lines is not None and self._cache_maxcol == maxcol:
            return
        self._stable_cache = False
        self._text = CommitFormatter.format_stat(self._stat, maxcol)
        self._attr = [((self._default_attr, 0),)] * len(self._text)
        super().build_line_cache(maxcol)


class Coupler(tuimatic.TextColumns):
    def __init__(self, *viewers):
        super().__init__(viewers, dividechars=1, sep='│')
        self._moving = False
        for viewer in viewers:
            tuimatic.connect_signal(viewer, 'moved', self._moved, weak_args=(viewer,))

    def _moved(self, source, x, y):
        if self._moving:
            return
        self._moving = True
        for viewer in self.contents:
            if viewer[0] == source:
                continue
            viewer[0].set_pref_text_cursor(x, y)
        self._moving = False


class DiffViewer(tuimatic.Viewer):
    def __init__(self, markup, line_pos_map=None):
        self.line_pos_map = line_pos_map
        super().__init__(markup, wrap='clip', tab_offset=7)

    def find_line_pos(self, *args):
        args = tuple(args)
        last = None
        fallback_last = None
        for i, line_pos in enumerate(self.line_pos_map):
            if line_pos is None:
                continue
            if line_pos == args:
                last = i
            elif line_pos[:2] == args[:2]:
                if line_pos[3] == args[3]:
                    # Workaround GitLab sometimes returning bogus old_line.
                    # Seen when commenting on a context line after a line
                    # that was added and removed in the same MR.
                    fallback_last = i
                elif line_pos[2] is None and line_pos[3] is None:
                    # match on file header as the last resort
                    fallback_last = i
        return last if last is not None else fallback_last

    def split(self, line):
        line += 1
        x, y = self.get_pref_text_cursor()

        new_viewer = DiffViewer('', self.line_pos_map[line:])
        new_viewer._text = self._text[line:]
        new_viewer._attr = self._attr[line:]
        cursor_pos = None
        if y >= line:
            cursor_pos = (x, y - line)
        new_viewer._apply_text_attr(cursor_pos)

        self.line_pos_map = self.line_pos_map[:line]
        self._text = self._text[:line]
        self._attr = self._attr[:line]
        self._apply_text_attr()

        return new_viewer


class ThreadAddError(Exception):
    pass


class DiffPile(tuimatic.TextPile):
    def __init__(self, formatted_diff):
        markup, line_pos_map = formatted_diff
        super().__init__(DiffViewer(markup, line_pos_map))

    def _find_line_pos(self, old_path, new_path, old_line, new_line):
        found = None
        for i, w in enumerate(self.contents):
            w = w[0]
            if not isinstance(w, DiffViewer):
                continue
            line = w.find_line_pos(old_path, new_path, old_line, new_line)
            if line is not None:
                found = (w, i, line)
        return found

    def get_line_pos_at_cursor(self):
        where = self.focus
        if not isinstance(where, DiffViewer):
            return None
        # assume wrap='clip'; text cursor y coordinate is the same as
        # rendered y coordinate
        line = where.get_pref_text_cursor(bounded=True)[1]
        return where.line_pos_map[line]

    def add_thread(self, widget, strict, old_path, new_path, old_line, new_line):
        """If strict is True, ThreadAddError is raised if the thread cannot
        be added precisely to the requested line pos."""
        found = self._find_line_pos(old_path, new_path, old_line, new_line)
        if found is None:
            if strict:
                raise ThreadAddError('No matching position')
            return False
        where, index, line = found
        index += 1
        if strict:
            line_pos = where.line_pos_map[line]
            if line_pos[2] is None and line_pos[3] is None:
                raise ThreadAddError('Matched only on path')
        if line == len(where.line_pos_map) - 1:
            # check if there are more threads for this line; if so, adjust
            # the index to come after them
            while (index < len(self.contents) and
                   not isinstance(self.contents[index][0], DiffViewer) and
                   not hasattr(self.contents[index][0], 'endsep')):
                index += 1
        else:
            # split the diff
            self.insert(index, where.split(line))
        if index < len(self.contents):
            if hasattr(self.contents[index][0], 'endsep'):
                self.set_enabled(index, True)
            else:
                sep = tuimatic.TextSeparator('─')
                sep.endsep = True
                self.insert(index, sep)
        self.insert(index, widget)
        return True

    def delete_thread(self, widget):
        for i, w in enumerate(self.contents):
            if w[0] == widget:
                break
        else:
            return
        if i == 0 or isinstance(self.contents[i - 1][0], DiffViewer):
            # no threads before us, delete endsep if it's after us
            if i < len(self.contents) - 1 and hasattr(self.contents[i + 1][0], 'endsep'):
                del self.contents[i + 1]
        elif not self.contents[i - 1][1]:
            # the previous thread is hidden, hide the endsep
            if i < len(self.contents) - 1 and hasattr(self.contents[i + 1][0], 'endsep'):
                self.set_enabled(i + 1, False)
        del self.contents[i]

    def get_threads(self, old_path, new_path, old_line, new_line):
        found = self._find_line_pos(old_path, new_path, old_line, new_line)
        if found is None:
            return []
        where, index, line = found
        index += 1
        if line != len(where.line_pos_map) - 1:
            return []
        result = []
        while (index < len(self.contents) and
               not isinstance(self.contents[index][0], DiffViewer) and
               not hasattr(self.contents[index][0], 'endsep')):
            result.append(self.contents[index][0])
            index += 1
        return result

    def show_threads(self, enable):
        show_endsep = False
        for i, w in enumerate(self.contents):
            w = w[0]
            if isinstance(w, DiffViewer):
                continue
            elif enable:
                self.set_enabled(i, True)
            elif hasattr(w, 'endsep'):
                self.set_enabled(i, show_endsep)
                show_endsep = False
            elif hasattr(w, 'comment_handler') and w.comment_handler.get_comment_editor():
                self.set_enabled(i, True)
                show_endsep = True
            else:
                self.set_enabled(i, False)


class DiffQuote(tuimatic.TextColumns):
    def __init__(self, diff_quote):
        sep = tuimatic.TextSeparator('┊', default_attr='diff-quote-char')
        super().__init__([(1, sep), DiffViewer(diff_quote)], dividechars=1)
