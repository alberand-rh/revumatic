import os
import signal
import tuimatic
import types
import weakref
from tuimatic.command_map import (CURSOR_LEFT, CURSOR_RIGHT, CURSOR_UP,
                               CURSOR_DOWN, CURSOR_MAX_LEFT, CURSOR_MAX_RIGHT,
                               CURSOR_PAGE_UP, CURSOR_PAGE_DOWN)

from modules.settings import settings


class clipboard:
    text = None


class EnhancedMainLoop(tuimatic.MainLoop):
    def __init__(self, *args, on_start=(), **kwargs):
        super().__init__(*args, **kwargs)
        self._on_start = on_start
        self.catch_signals()

    def catch_signals(self):
        def defer_to_sync(signalnum, stack):
            os.write(pipe, b'\0')

        def sigtstp(data):
            self.process_input(('ctrl z',))

        pipe = self.watch_pipe(sigtstp)
        self.event_loop.set_signal_handler(signal.SIGTSTP, defer_to_sync)

    def start(self):
        result = super().start()
        for func in self._on_start:
            func()
        return result


class MaxMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_size = None

    def set_max_size(self, index, max_size):
        """Set the max width/height of the widget at index. Only a single
        widget can have max size. Note that when redistributing the extra
        space, the weights are not taken into account. If we ever start
        using the weights, this would have to be fixed."""
        self.max_size = (index, max_size)
        self._invalidate()

    def adjust_size(self, sizes):
        if not self.max_size:
            return sizes

        index, max_size = self.max_size
        if sizes[index] <= max_size:
            return sizes

        to_enlarge = []
        for i, (_, s) in enumerate(self.contents):
            if i != index and s[0] not in (tuimatic.widget.GIVEN, tuimatic.widget.PACK):
                to_enlarge.append(i)

        add, rest = divmod(sizes[index] - max_size, len(to_enlarge))
        for i in to_enlarge:
            sizes[i] += add
            if rest:
                sizes[i] += 1
                rest -= 1
        sizes[index] = max_size
        return sizes


class ColumnsMax(MaxMixin, tuimatic.Columns):
    def column_widths(self, size, focus=False):
        widths = self.adjust_size(super().column_widths(size, focus))
        self._cache_column_widths = widths
        return widths


class PileMax(MaxMixin, tuimatic.Pile):
    def get_item_rows(self, size, focus):
        return self.adjust_size(super().get_item_rows(size, focus))


class Workplace(tuimatic.WidgetPlaceholder):
    signals = ['focus', 'help']

    def refocus(self):
        try:
            self.original_widget.base_widget.focus_position = 0
        except IndexError:
            return
        tuimatic.emit_signal(self, 'focus')

    def is_traversable(self, widget):
        """ListBox and TextBox are non-traversable. Meaning we always
        consider them as leaf when walking focus path. We do not descend
        into their content."""
        return not isinstance(widget, (tuimatic.ListBox, tuimatic.TextBox))

    def get_focus_path_w(self, deep=False, full=False):
        """A version of get_focus_path that returns also the widgets, that
        is, a list of tuples (widget, focus position in that widget).
        If deep is True, descend into non-traversable widgets. If
        full is True, include also decorations and the leaf widget in the
        result. The decorations and the leaf widget have None as the
        position. Setting full implies also deep."""
        if full:
            deep = True
        out = []
        w = self.original_widget
        while True:
            if not full:
                # skip decorations
                w = w.base_widget
            if not deep and not self.is_traversable(w):
                return out
            try:
                p = w.focus_position
            except IndexError:
                if full:
                    out.append((w, None))
                    if hasattr(w, 'original_widget'):
                        # this is a decoration, traverse it
                        w = w.original_widget
                        continue
                return out
            out.append((w, p))
            w = w.focus

    def set_focus_path_w(self, path, to_last=False):
        """A version of set_focus_path that accepts incomplete path. After
        path is consumed, it continues focusing widgets to their first
        selectable child (or the last selectable child if to_last is True).
        The focus continuation does not descend into non-traversable
        widgets."""
        w = self.original_widget.base_widget
        for _, p in path:
            if p != w.focus_position:
                w.focus_position = p
            w = w.focus.base_widget
        while True:
            if not self.is_traversable(w):
                return
            try:
                i = len(w.contents) - 1 if to_last else 0
                while True:
                    w.focus_position = i
                    if w.focus.selectable():
                        break
                    i = i - 1 if to_last else i + 1
            except (IndexError, AttributeError):
                return
            w = w.focus.base_widget

    def find_path_w(self, widget, deep=False):
        """Search for the given widget in the whole tree. If deep is True,
        descend into non-traversable widgets. Return the path to the widget
        (may be []) or None if the widget was not found."""
        path = []
        w, pos = self.original_widget.base_widget, 0
        while True:
            try:
                child = w.contents[pos][0]
            except (IndexError, TypeError, AttributeError):
                if len(path) == 0:
                    return None
                w, pos = path.pop()
                pos += 1
                continue
            while True:
                if child == widget:
                    path.append((w, pos))
                    return path
                if not hasattr(child, 'original_widget'):
                    break
                child = child.original_widget
            if not child.selectable():
                pos += 1
                continue
            if not deep and not self.is_traversable(child):
                pos += 1
                continue
            path.append((w, pos))
            w, pos = child, 0

    def focus_next(self, prev=False):
        path = self.get_focus_path_w()
        while len(path):
            w, p = path[-1]
            try:
                while True:
                    p = p - 1 if prev else p + 1
                    path[-1] = (w, p)
                    self.set_focus_path_w(path, prev)
                    if w.focus.selectable():
                        tuimatic.emit_signal(self, 'focus')
                        return
            except IndexError:
                path = path[:-1]
        self.set_focus_path_w(path, prev)
        tuimatic.emit_signal(self, 'focus')

    def focus_to(self, widget, deep=False):
        path = self.find_path_w(widget, deep=deep)
        if path is not None:
            self.set_focus_path_w(path)
            tuimatic.emit_signal(self, 'focus')
            return True
        return False

    def get_focus(self, deep=False):
        try:
            w, _ = self.get_focus_path_w(deep=deep)[-1]
        except IndexError:
            return None
        return w.focus.base_widget

    def get_help(self, long=False):
        data = {}
        for w, _ in reversed(self.get_focus_path_w(full=True)):
            w._command_map.collect_help(data)

        if not data:
            return ''

        result = []
        used_keys = set(settings.key_map['help'])
        if not long:
            result.append(('help-key', ' {} '.format(settings.key_map['help'][0])))
            result.append(('help-desc', ' help  '))
        for prio in sorted(data.keys(), reverse=True):
            for enabled, all_keys, desc, long_desc in data[prio]:
                if enabled:
                    keys = [key for key in all_keys if key not in used_keys]
                    used_keys.update(keys)
                    if not keys:
                        enabled = False
                if not enabled:
                    keys = all_keys
                if not long and not desc:
                    continue
                if not long and not enabled:
                    continue
                if long and not long_desc:
                    continue
                if long:
                    for i, key in enumerate(keys):
                        if i > 0:
                            result.append(('help-desc', ', '))
                        result.append(('help-key', ' {} '.format(key)))
                    state = '' if enabled else ' [disabled]'
                    result.append(('help-desc', ' {}{}\n'.format(long_desc, state)))
                else:
                    result.append(('help-key', ' {} '.format(keys[0])))
                    result.append(('help-desc', ' {}  '.format(desc)))
        return result

    def keypress(self, size, key):
        key = super().keypress(size, key)
        if key in settings.key_map['focus_next'] and self._command_map._all_enabled:
            self.focus_next()
            return None
        if key in settings.key_map['focus_prev'] and self._command_map._all_enabled:
            self.focus_next(prev=True)
            return None
        if key in settings.key_map['help'] and self._command_map._all_enabled:
            tuimatic.emit_signal(self, 'help', self.get_help(long=True) or [])
            return None
        return key


class PassiveAttrMap(tuimatic.AttrMap):
    def __init__(self, w, attr_map, focus_map, passive_map):
        self._passive = False
        super().__init__(w, attr_map, focus_map)
        self.set_passive_map(passive_map)

    def set_attr_map(self, attr_map):
        super().set_attr_map(attr_map)
        self._orig_attr_map = self._attr_map
        if self._passive:
            self._attr_map = self._passive_map

    def set_passive_map(self, passive_map):
        orig = self._focus_map
        super().set_focus_map(passive_map)
        self._passive_map = self._focus_map
        self._focus_map = orig

    def set_passive(self, enabled):
        if self._passive == enabled:
            return
        if enabled:
            self._attr_map = self._passive_map
        else:
            self._attr_map = self._orig_attr_map
        self._passive = enabled
        self._invalidate()


class SmartListWalker(tuimatic.SimpleFocusListWalker):
    """A list worker that remembers an arbitrary object for each items,
    supports a passive map and can be filtered."""

    # Note on self._focus and self._empty: MonitoredFocusList checks for self
    # being an empty list and sets self._focus to None in such case.
    # However, the filtering makes this more complicated. The list can be
    # effectively empty even though it contains items. For that effect, we
    # add self._empty that is used instead of checking whether self is
    # empty. We need to override _get_focus, _set_focus and
    # _adjust_focus_on_contents_modified and make them understand the new
    # logic.
    # When reading self.focus, None is returned when the list is empty or
    # completely filtered out. This is consistent with MonitoredFocusList
    # behavior.

    def __init__(self, content, focus_map, passive_map, formatter=None):
        """content is an iterable of objects. The objects are converted to
        a string by calling the formatter function, or by calling str() if
        formatter is None. If more control is desired, the formatter may
        return a widget instead of text."""
        self._filter = None
        self._down_after_delete = True
        self._focus_map = focus_map
        self._passive_map = passive_map
        if formatter is None:
            formatter = str
            self._formatter = None
        elif isinstance(formatter, types.MethodType):
            self._formatter = weakref.WeakMethod(formatter)
        else:
            self._formatter = weakref.ref(formatter)
        items = [(obj, self._build_item(formatter(obj))) for obj in content]
        self._ext_callback = None
        self._empty = len(content) == 0
        super().__init__(items)
        self._previous_passive = None
        self._set_passive(self.focus, True)

    def _build_item(self, c):
        if not isinstance(c, tuimatic.Widget):
            c = tuimatic.SelectableIcon(c)
            c.set_wrap_mode('ellipsis')
        return PassiveAttrMap(c, 'list', self._focus_map, self._passive_map)

    def _format_item(self, obj):
        formatter = None
        if self._formatter:
            formatter = self._formatter()
        if not formatter:
            formatter = str
        return formatter(obj)

    def append(self, obj):
        """Append a new object to the walker."""
        super().append((obj, self._build_item(self._format_item(obj))))

    def insert(self, index, obj):
        """Insert a new object to the walker at the given position."""
        super().insert(index, (obj, self._build_item(self._format_item(obj))))

    def get_object(self, index):
        """Get object at the given index."""
        return self[index][0]

    def get_focused_object(self):
        """Get object at the focused position."""
        return self.get_object(self.focus)

    def get_text(self, index):
        """Get text at the given index."""
        return self[index][1].original_widget.text

    def get_focused_text(self):
        """Get text at the focused position."""
        return self.get_text(self.focus)

    def get_text_widget(self, index):
        """Return the text widget at the given index."""
        return self.get_widget(index).original_widget

    def reformat_focused_text(self):
        """Refreshes the text at the focused position from its object."""
        obj, txt = self[self.focus]
        c = self._format_item(obj)
        if isinstance(c, tuimatic.Widget):
            txt.original_widget = c
        else:
            txt.original_widget.set_text(c)

    def set_direction_after_delete(self, down=True):
        """Sets the direction that the focus will move if the currently
        focused item is deleted (only deleted; not replaced with something
        else)."""
        self._down_after_delete = down

    def set_filter(self, func):
        """Set the filter that decides which items should be enabled. The
        function gets the object as the parameter and should return True if
        this object is enabled. To cancel the filter, pass None as the
        function."""
        self._filter = func
        self.reapply_filter()

    def reapply_filter(self):
        """Call when the filter changed but the filter function stays the
        same."""
        focus = self.focus
        empty = True
        lower = None
        found = None
        for pos in self.positions():
            empty = False
            if focus is None:
                # select the first item if the previous view was empty
                found = pos
                break
            if pos < focus:
                lower = pos
            elif pos >= focus:
                found = pos
                break
        self._empty = empty
        self.focus = found if found is not None else lower
        self._modified()

    def _adjust_focus_on_contents_modified(self, slc, new_items=()):
        start, stop, step = indices = slc.indices(len(self))
        if step != 1:
            raise IndexError('SmartListWalker does not support extended slices')

        first_new_focusable = None
        if self._filter:
            # Find the first non-filtered newly added item.
            for i, (obj, txt) in enumerate(new_items):
                if self._filter(obj):
                    first_new_focusable = i
                    break
            # Check whether there are any non-filtered items left after the
            # modification.
            deleted_all = first_new_focusable is None
            if deleted_all:
                for i in range(0, start):
                    if self._filter(self[i][0]):
                        deleted_all = False
                        break
            if deleted_all:
                for i in range(stop, len(self)):
                    if self._filter(self[i][0]):
                        deleted_all = False
                        break
        else:
            if new_items:
                first_new_focusable = 0
            deleted_all = start == 0 and stop == len(self) and not new_items

        focus = self._validate_contents_modified(indices, new_items)
        if focus is None:
            focus = self._focus
            if focus is None:
                if first_new_focusable is not None:
                    focus = start + first_new_focusable
            elif start <= focus < stop:
                if first_new_focusable is not None:
                    focus = start + first_new_focusable
                else:
                    if self._down_after_delete:
                        focus = self.next_position(stop - 1, default=None)
                        if focus is None:
                            focus = self.prev_position(start, default=None)
                    else:
                        focus = self.prev_position(start, default=None)
                        if focus is None:
                            focus = self.next_position(stop - 1, default=None)
                    # recalculate the found position to what it's going to
                    # be after the modification
                    if focus is not None and focus >= stop:
                        focus += len(new_items) - (stop - start)
            elif focus >= stop:
                focus += len(new_items) - (stop - start)

        # We're cheating here. If the actual list operation fails with an
        # exception and that exception is handled, the list will be left in
        # an inconsistent state. We should execute the commands below only
        # after the actual list operation. However, that would mean
        # overriding all list operations in this class.
        self._set_passive(self.focus, False)
        self._empty = deleted_all or (self._empty and first_new_focusable is None)
        return focus

    def _set_focus(self, index):
        if self._empty:
            self._focus = None
            return
        if index >= 0 and index < len(self):
            if self._filter and not self._filter(self[index][0]):
                raise IndexError('Item at position {} is disabled.'.format(index))
        super()._set_focus(index)

    focus = property(lambda self: self._get_focus(), _set_focus)

    def get_widget(self, index):
        return self[index][1]

    def get_focus(self):
        try:
            focus = self.focus
            return self[focus][1], focus
        except (IndexError, KeyError, TypeError):
            return None, None

    def next_position(self, position, default=IndexError):
        while position + 1 < len(self):
            position += 1
            if self._filter is None or self._filter(self[position][0]):
                return position
        if isinstance(default, type) and issubclass(default, Exception):
            raise default
        return default

    def prev_position(self, position, default=IndexError):
        while position - 1 >= 0:
            position -= 1
            if self._filter is None or self._filter(self[position][0]):
                return position
        if isinstance(default, type) and issubclass(default, Exception):
            raise default
        return default

    def get_next(self, position):
        try:
            position = self.next_position(position)
            return self[position][1], position
        except IndexError:
            return None, None

    def get_prev(self, position):
        try:
            position = self.prev_position(position)
            return self[position][1], position
        except IndexError:
            return None, None

    def positions(self, reverse=False, start=None, stop=None):
        """Returns iterator over all non-filtered positions. The positions
        will be in the range of `(start, stop)`. They will either go from
        `start` to `stop - 1` if `reverse` is False, of from `stop - 1` to
        `start` otherwise."""
        start = 0 if (start is None or start < 0) else start
        stop = len(self) if (stop is None or stop > len(self)) else stop
        if reverse:
            what = range(stop - 1, start - 1, -1)
        else:
            what = range(start, stop)
        for i in what:
            if self._filter is None or self._filter(self[i][0]):
                yield i

    def objects(self):
        """Returns an iterator over objects of all enabled (not filtered
        out) items."""
        for obj, txt in self:
            if self._filter is None or self._filter(obj):
                yield obj

    def len(self):
        """Returns the number of enabled (not filtered out) items."""
        return sum(self._filter is None or self._filter(obj) for obj, _ in self)

    def _set_passive(self, index, enabled):
        if enabled and self._previous_passive is not None:
            self._previous_passive.set_passive(False)
            self._previous_passive = None
        if index is None:
            return
        widget = self[index][1]
        if hasattr(widget, 'set_passive'):
            widget.set_passive(enabled)
            if not enabled and self._previous_passive == widget:
                self._previous_passive = None
            if enabled:
                self._previous_passive = widget

    def _focus_changed(self):
        self._set_passive(self.focus, True)
        if self._ext_callback:
            self._ext_callback()
        super()._focus_changed()

    def set_focus_changed_callback(self, callback):
        self._ext_callback = callback


class JailedPlaceholder(tuimatic.WidgetPlaceholder):
    """A placeholder widget that does not allow arrow keys to escape. Used
    as a container for widgets that should be focused using the Tab key
    only."""
    def keypress(self, size, key):
        key = super().keypress(size, key)
        if self._command_map[key] in (CURSOR_LEFT,
                                      CURSOR_RIGHT,
                                      CURSOR_UP,
                                      CURSOR_DOWN,
                                      CURSOR_MAX_LEFT,
                                      CURSOR_MAX_RIGHT,
                                      CURSOR_PAGE_UP,
                                      CURSOR_PAGE_DOWN):
            key = None
        return key


class FixedButton(tuimatic.Button):
    def pack(self, size, focus=False):
        return (len(self._label.text) + 4, 1)


class ToggleCheckBox(tuimatic.CheckBox):
    def keypress(self, size, key):
        if key not in settings.key_map['toggle']:
            return key
        self.toggle_state()


class Popup(tuimatic.Overlay):
    def __init__(self, callback, workplace, attr_map=None, focus_map=None, height=None,
                 callback_args=None, popup_widget=None, overlay_attr_map=None):
        self._workplace = workplace
        self._attr_map = attr_map
        self._focus_map = focus_map
        self._callback = callback
        self._callback_args = callback_args or ()
        self._default_button = None
        self._default_esc_button = None
        self._shortcuts = {}
        self._data_widgets = {}
        if height == 'max':
            parms = { 'align': 'center',
                      'width': ('relative', 50),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height == 'full':
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height is None:
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': 'pack',
                    }
        else:
            if type(height) == int:
                height = height + 2
            else:
                assert(height == 'pack')
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 0),
                      'min_height': height,
                    }
        super().__init__(None, self._workplace.original_widget,
                         attr_map=overlay_attr_map, **parms)
        if popup_widget:
            self._setup(popup_widget)

    def new_button(self, label):
        if type(label) == str:
            index = label.find('~')
            if index >= 0:
                shortcut = label[index + 1]
                label = [label[:index], ('button-shortcut', shortcut), label[index+2:]]
                self._shortcuts[shortcut] = tuimatic.MarkupFormat(label).decompose()[0][0]
        button = FixedButton(label)
        tuimatic.connect_signal(button, 'click', self._handler)
        return tuimatic.AttrMap(button, self._attr_map, self._focus_map)

    def new_edit(self, name, edit_text, **kwargs):
        widget = tuimatic.Editor(edit_text, **kwargs)
        self._data_widgets[name] = widget
        return widget

    def register_widget(self, name, widget):
        self._data_widgets[name] = widget
        return widget

    def set_default_button(self, label, label_esc=None):
        """Sets the button that is reported when Enter is pressed in one of
        the edit boxes. Similarly for Esc. Note that the label does not need
        to correspond to a real button. As a special case, if label or
        label_esc is True, the Enter/Esc is functional but the button label
        is not reported to the callback. Edit data is reported to the
        callback only if a non-Esc button was pressed."""
        self._default_button = label
        self._default_esc_button = label_esc

    def _setup(self, widget):
        self.popup_widget = widget
        self.top_w = tuimatic.LineBox(widget)

    def show(self):
        self._workplace.original_widget = self
        self._workplace._command_map.set_all_actions_enabled(False)

    def start(self, widget):
        self._setup(widget)
        self.show()

    def _stop(self):
        self._workplace.original_widget = self.contents[0][0]
        self._workplace._command_map.set_all_actions_enabled(True)

    def stop(self):
        self._stop()
        self._callback(*self._callback_args)

    def keypress(self, size, key):
        if key == 'enter':
            focus = self.get_focus_widgets()
            handle = not focus
            for f in focus:
                f = f.base_widget
                if f in self._data_widgets.values():
                    handle = f
                    break
            if handle and self._handler(key=key, focus=f if handle is not True else None):
                return None
        elif key == 'esc':
            if self._handler(key=key):
                return None
        elif key in self._shortcuts:
            if self._handler(key=key):
                return None
        return super().keypress(size, key)

    def _handler(self, button=None, key=None, focus=None):
        collect_data = True
        if button is not None:
            button = tuimatic.MarkupFormat(button.label).decompose()[0][0]
            if button == self._default_esc_button:
                collect_data = False
        if key is not None:
            if key == 'enter' and self._default_button:
                button = self._default_button
            elif key == 'esc' and self._default_esc_button:
                button = self._default_esc_button
                collect_data = False
            elif key in self._shortcuts:
                button = self._shortcuts[key]
                if button == self._default_esc_button:
                    collect_data = False
            else:
                return False

        self._stop()
        data = {}
        if collect_data:
            for k, widget in self._data_widgets.items():
                if isinstance(widget, tuimatic.Editor):
                    data[k] = widget.get_text()
                elif isinstance(widget, tuimatic.Pile) and widget == focus:
                    data[k] = widget.focus_position
                elif isinstance(widget, tuimatic.ListBox):
                    data[k] = widget.body.get_focused_text()
        if button is not None and button is not True:
            data['button'] = button
        self._callback(*self._callback_args, **data)
        return True
