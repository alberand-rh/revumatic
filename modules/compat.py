# This file contains definitions to preserve backwards compatibility.
import os

# Mapping from old action names to the currently used ones. This is used
# when reading the [keys] section of the config file.
compat_actions = {
    'interdiff_alt': None,
    'upstream': 'interdiff',
    'upstream_alt': None,
    'select': 'selection_start',
    'bugzilla': 'bug_tracker',
    'find_file_next': 'file_owners_filter',
    'find_file_prev': 'file_filter',
}

# Detection of file names of the old comment storage format.
def is_compat_comment(name):
    return name == 'general' or (len(name) == 40 and
           name.translate(str.maketrans('', '', '0123456789abcdef')) == '')

# Comment conversion from the old storage to the new json based storage.
def convert_comment(dir_fd, filename, storage):
    data = {}
    with open(os.open(filename, os.O_RDONLY, dir_fd=dir_fd), 'r') as f:
        data['subject'], data['upstream'], data['text'] = f.read().split('\n', maxsplit=2)
    if filename == 'general':
        del data['subject']
        del data['upstream']
        data['thread_id'] = 'new'
    else:
        data['commit'] = filename
    storage.save(data)
    os.remove(filename, dir_fd=dir_fd)
    return data
