import datetime
import json
import os

from modules.settings import settings
from modules import compat, gitlab

class CommentStorage:
    def __init__(self, repo, iid):
        self.subpath = os.path.join('comments', repo.project.replace('/', '-'), str(iid))
        self.next_id = self._last_id() + 1

    def __getitem__(self, cmt_id):
        try:
            with settings.dir.open(os.path.join(self.subpath, 't{}'.format(cmt_id))) as f:
                data = json.load(f)
                assert data['id'] == int(cmt_id)
                return data
        except FileNotFoundError:
            raise KeyError('comment entry {} not present'.format(cmt_id))

    def __delitem__(self, data):
        if data.get('id') is None:
            return
        try:
            settings.dir.remove(os.path.join(self.subpath, 't{}'.format(data['id'])))
        except FileNotFoundError:
            return
        self.cleanup()

    def save(self, data):
        if data.get('id') is None:
            data['id'] = self.next_id
            self.next_id += 1
        settings.dir.makedirs(self.subpath)
        with settings.dir.rewrite(os.path.join(self.subpath, 't{}'.format(data['id']))) as f:
            json.dump(data, f)

    def __iter__(self):
        try:
            dir_fd = settings.dir.open_fd(self.subpath)
        except FileNotFoundError:
            return
        try:
            for name in sorted(os.listdir(dir_fd)):
                if compat.is_compat_comment(name):
                    yield compat.convert_comment(dir_fd, name, self)
                if name.startswith('t'):
                    yield self[name[1:]]
        finally:
            os.close(dir_fd)

    def _last_id(self):
        last = 0
        try:
            dir_fd = settings.dir.open_fd(self.subpath)
        except FileNotFoundError:
            return 0
        try:
            for name in os.listdir(dir_fd):
                if not name.startswith('t'):
                    continue
                try:
                    last = max(last, int(name[1:]))
                except ValueError:
                    pass
        finally:
            os.close(dir_fd)
        return last

    def cleanup(self):
        settings.dir.removedirs(self.subpath)


class HistoryStorage:
    max_length = 100
    min_days = 14
    file_name = 'history'

    def load(self):
        try:
            with settings.dir.open(self.file_name) as f:
                return json.load(f)
        except FileNotFoundError:
            return []

    def trim(self, data, now):
        where = len(data) - self.max_length
        while (where > 0 and
               (now - gitlab.parse_datetime(data[where - 1]['date'])).days < self.min_days):
            where -= 1
        if where <= 0:
            return data
        return data[where:]

    def add_mr(self, mr, body, blocking_callback=None, blocking_parms=(),
               minor=False):
        fields = ('id', 'iid', 'web_url', 'created_at', 'updated_at', 'target_branch',
                  'title')
        now = datetime.datetime.now().astimezone()
        record = {}
        record['body'] = body
        record['date'] = gitlab.encode_datetime(now)
        if minor:
            record['minor'] = True
        record['mr'] = { k: mr[k] for k in fields }
        record['mr']['author'] = { 'name': mr['author']['name'] }
        record['mr']['references'] = { 'full': mr['references']['full'] }

        with settings.dir.lock(self.file_name):
            data = self.load()
            data.append(record)
            with settings.dir.rewrite(self.file_name) as f:
                json.dump(self.trim(data, now), f)
