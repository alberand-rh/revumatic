#!/bin/bash
set -e

function get_version
{
	baseversion=$(date -d "$1" '+%y.%m.%d')
	extraversion=0
	version=$baseversion
	while git rev-parse "v$version" >& /dev/null; do
		extraversion=$((extraversion + 1))
		version=$baseversion.$extraversion
	done
	echo "$version"
}

function abort
{
	[[ -z $msgfile ]] || rm -f "$msgfile"
	if [[ $undo ]]; then
		echo "UNDO BY:"
		for u in "${undo[@]}"; do
			echo $u
		done >&2
	fi
}

undo=()
msgfile=
trap abort EXIT

dirty=$(git status --short --untracked-files=no)
if [[ $dirty ]]; then
	echo "The git tree must be clean." >&2
	exit 1
fi

version=$(get_version now)
devversion=$(get_version tomorrow)
prevmain=$(git rev-parse --short main)
prevnext=$(git rev-parse --short next)
git checkout main
undo=('git merge --abort')
git merge next
undo=('git checkout main' "git reset --hard $prevmain")
sed -i -e "s/^version = .*/version = '$version'/" \
       -e "s/^devel = .*/devel = False/" \
    modules/version.py
msgfile=$(mktemp)
cat > "$msgfile" <<EOT
v$version

# git log $prevmain..$prevnext
News:
- [new] 
- [improvement] 
- [change] 
- [fix] 

EOT
git commit -a -s -t "$msgfile"
git log -1 --pretty=format:%B HEAD > "$msgfile"
git tag -a -F "$msgfile" "v$version"
undo=("git tag -d v$version" "${undo[@]}")

git checkout next
git merge main
undo+=('git checkout next' "git reset --hard $prevnext")
sed -i -e "s/^version = .*/version = '$devversion'/" \
       -e "s/^devel = .*/devel = True/" \
    modules/version.py
git commit -a -s -m 'Start the next version development'

prevrpm=$(git rev-parse --short rpm)
rpmdate=$(LC_TIME=C date '+%a %b %d %Y')
author="$(git config user.name) <$(git config user.email)>"
git checkout rpm
undo+=('git checkout rpm' "git reset --hard $prevrpm")
sed -i -e "s/^Version: .*/Version: $version/" \
       -e "/^%changelog/a\\
* $rpmdate $author - $version-1\\
- version update\\
" revumatic.spec
git commit -a -s -m "rpm: v$version"
git checkout main
echo "All okay. Don't forget to push the changes."
