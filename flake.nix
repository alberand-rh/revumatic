{
  description = "Revumatic";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      revumatic-drv = pkgs.python311Packages.buildPythonApplication {
        name = "revumatic";

        src = ./.;

        propagatedBuildInputs = with pkgs.python311Packages; [
          pygit2
          levenshtein
          pyyaml
          urllib3
        ];

        doCheck = false;
      };
    in {
      packages = rec {
          revumatic = revumatic-drv;
          default = revumatic-drv;
      };

      apps = rec {
        revumatic = flake-utils.lib.mkApp {
          drv = self.packages.${system}.revumatic;
        };
        default = revumatic;
      };
    });
}
