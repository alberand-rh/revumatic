import re

# Unicode "fullwidth" characters. These are printed as double width in the console.
FULLWIDTH = re.compile(r'[\u1100-\u115f\u231a-\u231b\u2329-\u232a\u23e9-\u23ec\u23f0-\u23f0\u23f3-\u23f3\u25fd-\u25fe\u2614-\u2615\u2648-\u2653\u267f-\u267f\u2693-\u2693\u26a1-\u26a1\u26aa-\u26ab\u26bd-\u26be\u26c4-\u26c5\u26ce-\u26ce\u26d4-\u26d4\u26ea-\u26ea\u26f2-\u26f3\u26f5-\u26f5\u26fa-\u26fa\u26fd-\u26fd\u2705-\u2705\u270a-\u270b\u2728-\u2728\u274c-\u274c\u274e-\u274e\u2753-\u2755\u2757-\u2757\u2795-\u2797\u27b0-\u27b0\u27bf-\u27bf\u2b1b-\u2b1c\u2b50-\u2b50\u2b55-\u2b55\u2e80-\u2e99\u2e9b-\u2ef3\u2f00-\u2fd5\u2ff0-\u2ffb\u3000-\u3004\u3006-\u3029\u3030-\u3030\u3036-\u303a\u303c-\u303e\u3041-\u3096\u309b-\u309c\u309f-\u30fb\u30ff-\u30ff\u3105-\u312f\u3131-\u318e\u3190-\u31e3\u31f0-\u321e\u3220-\u3247\u3250-\u4dbf\u4e00-\ua014\ua016-\ua48c\ua490-\ua4c6\ua960-\ua97c\uac00-\ud7a3\uf900-\ufa6d\ufa70-\ufad9\ufe10-\ufe19\ufe30-\ufe52\ufe54-\ufe66\ufe68-\ufe6b\uff01-\uff60\uffe0-\uffe6\U00016fe2-\U00016fe2\U00017000-\U000187f7\U00018800-\U00018cd5\U00018d00-\U00018d08\U0001b000-\U0001b122\U0001b132-\U0001b132\U0001b150-\U0001b152\U0001b155-\U0001b155\U0001b164-\U0001b167\U0001b170-\U0001b2fb\U0001f004-\U0001f004\U0001f0cf-\U0001f0cf\U0001f18e-\U0001f18e\U0001f191-\U0001f19a\U0001f200-\U0001f202\U0001f210-\U0001f23b\U0001f240-\U0001f248\U0001f250-\U0001f251\U0001f260-\U0001f265\U0001f300-\U0001f320\U0001f32d-\U0001f335\U0001f337-\U0001f37c\U0001f37e-\U0001f393\U0001f3a0-\U0001f3ca\U0001f3cf-\U0001f3d3\U0001f3e0-\U0001f3f0\U0001f3f4-\U0001f3f4\U0001f3f8-\U0001f43e\U0001f440-\U0001f440\U0001f442-\U0001f4fc\U0001f4ff-\U0001f53d\U0001f54b-\U0001f54e\U0001f550-\U0001f567\U0001f57a-\U0001f57a\U0001f595-\U0001f596\U0001f5a4-\U0001f5a4\U0001f5fb-\U0001f64f\U0001f680-\U0001f6c5\U0001f6cc-\U0001f6cc\U0001f6d0-\U0001f6d2\U0001f6d5-\U0001f6d7\U0001f6dc-\U0001f6df\U0001f6eb-\U0001f6ec\U0001f6f4-\U0001f6fc\U0001f7e0-\U0001f7eb\U0001f7f0-\U0001f7f0\U0001f90c-\U0001f93a\U0001f93c-\U0001f945\U0001f947-\U0001f9ff\U0001fa70-\U0001fa7c\U0001fa80-\U0001fa88\U0001fa90-\U0001fabd\U0001fabf-\U0001fac5\U0001face-\U0001fadb\U0001fae0-\U0001fae8\U0001faf0-\U0001faf8\U00020000-\U0002a6df\U0002a700-\U0002b739\U0002b740-\U0002b81d\U0002b820-\U0002cea1\U0002ceb0-\U0002ebe0\U0002f800-\U0002fa1d\U00030000-\U0003134a\U00031350-\U000323af]')
TAB_RE = re.compile(r'\t\x01*')


fullwidth_supported = True
def disable_fullwidth():
    global fullwidth_supported
    fullwidth_supported = False


def text_format(data):
    if isinstance(data, BaseFormat):
        return data
    return MarkupFormat(data)


def raw_text(text):
    """Remove the special control characters ('\x00' and '\x01') from the
    decomposed text."""
    return text.translate([ None, None ])


def retab(text, attr, tab_width=8, tab_offset=0):
    """Fixes tab widths in the given text. Returns a new (text, attr)."""

    def copy_attrs(pos):
        nonlocal attr_pos

        while attr_pos < len(attr) and attr[attr_pos][1] <= pos:
            nattr.append((attr[attr_pos][0], attr[attr_pos][1] + extra_chars))
            attr_pos += 1

    def expand_tab(match):
        nonlocal extra_chars

        copy_attrs(match.start())
        add = tab_width - (tab_offset + extra_chars + match.start()) % tab_width - 1
        result = '\t' + '\x01' * add
        extra_chars += add - len(match.group()) + 1
        return result

    extra_chars = 0
    attr_pos = 0
    nattr = []
    ntext, count = TAB_RE.subn(expand_tab, text)
    if not count:
        return text, attr
    copy_attrs(len(text))
    return ntext, nattr


def split_text_attr_line(text, attr, pos):
    """Splits the given 'text' (a string representing a single line) and its
    corresponding 'attr' at the 'pos'. Returns a tuple of (ltext, lattr, rtext,
    rattr)."""
    lattr = []
    rattr = []
    if pos == 0:
        # need to have at least one attr even for empty text
        lattr.append((None, 0))
    i = 0
    while i < len(attr) and attr[i][1] < pos:
        lattr.append(attr[i])
        i += 1
    if i == len(attr) or attr[i][1] > pos:
        # splitting in the middle of an attr; duplicate the attr
        rattr.append((attr[i-1][0], 0))
    while i < len(attr):
        # also adjust the offsets
        rattr.append((attr[i][0], attr[i][1] - pos))
        i += 1
    return text[:pos], lattr, text[pos:], rattr


def combine_text_attr_line(text1, attr1, text2, attr2):
    """Combines the given texts and their attrs to a single line. Returns
    a tuple of (text, attr). BEWARE, it modifies attr1."""
    i = 0
    if attr1[-1][0] == attr2[0][0]:
        # attr1 ends with the same attr that attr2 starts with; combine them
        i = 1
    add = len(text1)
    while i < len(attr2):
        attr1.append((attr2[i][0], attr2[i][1] + add))
        i += 1
    return text1 + text2, attr1


class FormatException(Exception):
    pass


class BaseFormat:
    """Base class to derive formatters from. Handles plain strings only."""

    def __init__(self, data):
        """data is class specific input data."""
        self.data = data

    def _line_init(self):
        self._cur_line_text = []
        self._cur_line_attr = [(self.default_attr, 0)]
        self._cur_line_offset = 0
        self._cur_line_length = 0

    def _expand_tab(self, m):
        add = (self.tab_width -
               (self.tab_offset + self._cur_line_length + self._extra_chars +
                m.start()) % self.tab_width - 1)
        result = '\t' + '\x01' * add
        self._extra_chars += add
        return result

    def _add_chunk(self, text, attr):
        """Adds a text chunk with the given attr to the output. The text
        chunk must not contain a newline."""
        if not text:
            return
        if fullwidth_supported:
            text = FULLWIDTH.sub(lambda match: match.group() + '\x00', text)
        else:
            text = FULLWIDTH.sub('\ufffd', text)
        self._extra_chars = 0
        text = re.sub('\t', self._expand_tab, text)
        self._cur_line_text.append(text)
        self._cur_line_length += len(text)
        if self._cur_line_attr[-1][0] != attr:
            # do not repeat the attr if it's the same as the previous one
            if self._cur_line_attr[-1][1] == self._cur_line_offset:
                # overwrite a zero length attr
                self._cur_line_attr[-1] = (attr, self._cur_line_offset)
            else:
                self._cur_line_attr.append((attr, self._cur_line_offset))
        self._cur_line_offset += len(text)

    def _next_line(self):
        """Adds a newline to the output. This should strictly correspond to
        the \n characters in the input, i.e. do not call this at the end of
        input if there's not a newline character. (_decompose_finish will
        take care of handling the end of input properly.)"""
        self._text.append(''.join(self._cur_line_text))
        self._attr.append(self._cur_line_attr)
        self._line_init()

    def _add_segment(self, text, attr):
        """Adds a text segment with the given attr to the output. The text
        segment may contain newlines."""
        for i, t in enumerate(text.split('\n')):
            if i > 0:
                self._next_line()
            self._add_chunk(t, attr)

    def _decompose_init(self):
        self._text = []
        self._attr = []
        self._line_init()

    def _decompose_finish(self):
        if self._cur_line_text or not self._text or self.keep_end_nl:
            # If there's unprocessed data (meaning there was no \n at the
            # end of the user supplied data), flush them; also flush the
            # (empty) data if the user supplied data were empty. If
            # keep_end_nl is True, always flush.
            self._next_line()

    def _decompose(self):
        self._add_segment(self.data, self.default_attr)

    def decompose(self, tab_width=8, tab_offset=0, keep_end_nl=False, default_attr=None):
        """Process the input and return two lists: a list of strings, one for
        each line, and a list of corresponding attributes, one list for each
        line. The attribute list contains (attr, offset) tuples. The offset
        points to the character in the corresponding line where the given
        attr starts. The first item in each attribute list must have always
        offset 0.

        Parameters: tab_width is tab stop interval for tab expansion.
        tab_offset is the number of characters that are pretended to be
        prefixed to each line for the purpose of tab expansion. If
        keep_end_nl is True and the input ends with a new line, there will
        be an additional empty line in the decomposed output. default_attr
        will be used for all parts of the text where the attribute is
        unspecified."""
        self.tab_width = tab_width
        self.tab_offset = tab_offset
        self.keep_end_nl = keep_end_nl
        self.default_attr = default_attr

        self._decompose_init()
        self._decompose()
        self._decompose_finish()
        return self._text, self._attr

    @classmethod
    def compose_line(cls, text, attr):
        """Converts the given decomposed line to the input format."""
        return raw_text(text)

    def append(self, data):
        """Append the given input data to the current input data."""
        self.data += data

    def prepend(self, data):
        """Prepend the given input data before the current input data."""
        self.data = data + self.data


class MarkupFormat(BaseFormat):
    """Handles the default tuimatic markup format, that is:
    STRING := str | (attr, STRING) | [STRING, STRING...]"""

    def _decompose(self):
        stack = [(self.default_attr, self.data, 0)]
        while stack:
            attr, current, index = stack.pop()

            if isinstance(current, str):
                self._add_segment(current, attr)
                continue

            if isinstance(current, tuple):
                if len(current) != 2:
                    raise FormatException("Tuples must be in the form (attribute, tagmarkup): {}"
                                          .format(current))
                stack.append((current[0], current[1], 0))
                continue

            if isinstance(current, list):
                if index >= len(current):
                    continue
                stack.append((attr, current, index + 1))
                stack.append((attr, current[index], 0))
                continue

            raise FormatException("Invalid markup element: {}".format(current))

    @classmethod
    def compose_line(cls, text, attr):
        result = []
        for i, (a, offset) in enumerate(attr):
            if i + 1 == len(attr):
                end_offset = len(text)
            else:
                end_offset = attr[i + 1][1]
            result.append((a, raw_text(text[offset:end_offset])))
        return result

    def append(self, data):
        self.data = [self.data, data]

    def prepend(self, data):
        self.data = [data, self.data]


class StrPos(str):
    def __new__(cls, s, pos):
        obj = super().__new__(cls, s)
        obj.pos = pos
        return obj


class TerminalFormat(BaseFormat):
    """Handles \r and \b in the text. Attributes are not supported,
    everything is rendered with the default_attr."""

    def _line_init(self):
        super()._line_init()
        self.pos = 0

    def _next_line(self):
        self._text.append(StrPos(''.join(self._cur_line_text), self.pos))
        self._attr.append(self._cur_line_attr)
        self._line_init()

    def _decompose(self):
        for i, t in enumerate(self.data.split('\n')):
            if i > 0:
                self._next_line()
            out = []
            for ch in t:
                if ch == '\b':
                    self.pos = max(0, self.pos - 1)
                elif ch == '\r':
                    self.pos = 0
                else:
                    if self.pos == len(out):
                        out.append(ch)
                    else:
                        out[self.pos] = ch
                    self.pos += 1
            self._add_chunk(''.join(out), self.default_attr)

    @classmethod
    def compose_line(cls, text, attr):
        if isinstance(text, StrPos) and text.pos < len(text):
            return raw_text(text) + '\b' * (len(text) - text.pos)
        return raw_text(text)
